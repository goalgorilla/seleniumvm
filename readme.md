Seleniumvm
==========
Seleniumvm is a Puppet-based Vagrant box which you can use for Selenium testing. Tested in combination with Behat.

Installation
------------
  1. Put your configuration in config.json
  2. Run `vagrant up`
  3. Done!

Verify selenium works
---------------------
Visit http://localhost:4444/wd/hub in a browser. You should see the default Selenium interface.

Verify chrome works
-------------------
Run the vagrantTest.js file with node. Make sure selenium-webdriver is installed.

  1. `npm install selenium-webdriver`
  2. `node vagrantTest.js`
  3. You should see the printed out HTML.

Credits
-------
Used the following tutorial by Chris Le http://www.chrisle.me/2013/08/running-headless-selenium-with-chrome/

Edited by GoalGorilla.