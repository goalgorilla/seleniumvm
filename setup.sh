#!/bin/sh
set -e

if [ -e /.installed ]; then
  echo 'Already installed.'

else

  echo ''
  echo 'INSTALLING'
  echo '----------'

  # Add Google public key to apt
  wget -q -O - "https://dl-ssl.google.com/linux/linux_signing_key.pub" | sudo apt-key add -

  # Add Google to the apt-get source list
  echo 'deb http://dl.google.com/linux/chrome/deb/ stable main' >> /etc/apt/sources.list

  # Update app-get
  apt-get update

  # Install Java, Chrome, Xvfb, and unzip
  apt-get -y install openjdk-7-jre google-chrome-stable xvfb unzip curl

  # Configure timezone:
  # dpkg-reconfigure tzdata

  HOSTS=$(echo "$1")
  echo "$HOSTS" >> /etc/hosts

  ln -s /vagrant/bin/chromedriver /usr/local/bin/chromedriver
  ln -s /vagrant/bin/selenium-server-standalone-2.44.0.jar /usr/local/bin/selenium-server-standalone-2.44.0.jar

  # So that running `vagrant provision` doesn't redownload everything
  touch /.installed

fi

# RUN THIS AS ROOT IN THE BOX TO START A NEW SERVER:

# Start Xvfb, Chrome, and Selenium in the background
export DISPLAY=:10
cd /vagrant
echo "Starting Xvfb ..."
Xvfb :10 -screen 0 1366x768x24 -ac &

echo "Starting Google Chrome ..."
google-chrome --remote-debugging-port=9222 &

echo "Starting Selenium ..."
cd /usr/local/bin
nohup java -jar ./selenium-server-standalone-2.44.0.jar &

echo "========================================================================"
echo "Visit http://localhost:4444/wd/hub in a browser."
echo "OR if you changed the host_port to something else use the given port."
echo "========================================================================"

